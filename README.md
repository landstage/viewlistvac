Интерфейс просмотра списка вакансий
=================
Интерфейс просмотра списка вакансий компании с возможностью фильтра по отделам и языку. Каждая вакансия определена следующими свойствами: отдел, название вакансии и ее описание. Отдел является отдельным объектом, представлен идентификатором и названием. При этом название вакансии и её описание могут быть переведены на несколько языков (ru, fr, it, ...).

Минимальные требования, для работы проекта
------------
* PHP >= 5.4
* MySQL 5.x
* Nginx
* Linux сервер, с возможностью root доступа 

Возможности
------------

* Просмотра списка вакансий.
* Поддержка фильтра по отделам и языку.
* Проверка на наборе тестовых данных.


Настройка окружения
------------
* Настройка ngnix, php-fpm

Создать виртуальный хост в директории sites-available со следующим содержанием:

```
	server {
		listen		80;
		server_name	vac.local;

		set		$root_path '/home/necrom/Projects/PHP/vac/public';

		charset		utf8;
		access_log	/home/necrom/Projects/PHP/log/vac/access.log main;
		error_log	/home/necrom/Projects/PHP/log/vac/error.log;
		root		$root_path;

		index index.php index.htm index.html;

		# Только для отладки
		fastcgi_intercept_errors Off;
		location / {
			try_files $uri $uri/ /index.php;
		}

		location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
			expires 24h;
			log_not_found off;
		}

		# передаем PHP-скрипт серверу FastCGI
		location ~ \.php {
			try_files	$uri =404;
			include		fastcgi_params;

			fastcgi_index	/index.php;
			fastcgi_pass    unix:/var/run/php5-fpm.sock;

			# Разбивает адресную строку запроса на 2 части,
			# Первая часть в переменную $fastcgi_script_name
			# Вторая часть в $fastcgi_path_info
			fastcgi_split_path_info ^(.+\.php)(/.+)$;
			fastcgi_param	PATH_INFO       $fastcgi_path_info;
			fastcgi_param	SCRIPT_FILENAME  $document_root$fastcgi_script_name;
			fastcgi_param APPLICATION_ENV "development";
		}

		# не позволять nginx отдавать файлы, начинающиеся с точки (.htaccess, .svn, .git и прочие)
		location ~ /\. {
			deny all;
			access_log off;
			log_not_found off;
		}

		location ~* ^/(css|img|js|flv|swf|download)/(.+)$ {
			root $root_path;
		}
	}
```

Создать символическую ссылку на виртуальный хост ln -s <параметры>

Пример:

```
	ln -s /etc/nginx/sites-available/vac.local /etc/nginx/sites-enabled/vac.local
```
Перезагрузить конфигурацию nginx

Добавить в host файл.

Развёртывания проекта
------------

* Склонировать проект


```
#!sh

    cd my/project/dir
    git clone https://landstage@bitbucket.org/landstage/viewlistvac.git
    cd viewlistvac
    php composer.phar self-update
```



* Установить зависимости через composer

`
    php composer.phar install
`

+ Создать конфигурационные файлы для приложения
`
touch my/project/dir/config/autoload/doctrine.local.php
`

* doctrine.local.php - указать надстроечные параметры для БД (host, port, user, password)


```
#!php

<?php
	    return array(
	        'doctrine' => array(
	            'connection' => array(
	                'orm_default' => array(
	                    'driverClass' =>'Doctrine\DBAL\Driver\PDOMySql\Driver',
	                    'params' => array(
	                        'host'     => 'localhost',
	                        'port'     => '3306',
	                        'user'     => 'root',
	                        'password' => '',
	                        'dbname'   => 'enterprise',
	                    )
	                )
	            ),
	        ),
	    );
```


* zenddevelopertools.local.php

`
cd my/project/dir/
cp ./vendor/zendframework/zend-developer-tools/config/zenddevelopertools.local.php.dist my/project/dir/config/autoload/ 
`

* Создать БД следующим запросом

         CREATE DATABASE IF NOT EXISTS `enterprise` /*!40100 DEFAULT CHARACTER SET utf8 */;

* Развернуть структуру БД используя консоль, следующей командой
 `
    ./bin/doctrine-module orm:schema-tool:create 
 `
 
 * Добавить данные для БД, следующей командой
`
 ./bin/doctrine-module data-fixture:import 
`
  
 * На папку data установить права доступа
  `
    chmod -R 775 ./data/
 `
 
 * Проверить установлено ли расширение на php, для работы языка (игрался с переводом на другие языки)
  `
  apt-get install php5-intl
 `

## Тестировать программу