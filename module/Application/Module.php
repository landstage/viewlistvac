<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use \Zend\Session\Container;

class Module
{
	public function onBootstrap(MvcEvent $e) {
		$eventManager = $e->getApplication()->getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener->attach($eventManager);


		// @TODO не работает нужно разобраться
		//$eventManager->attach(MvcEvent::EVENT_ROUTE, array($this, 'initTranslator'), 1);
		//$this->initTranslator($e);
	}


	protected function initTranslator(MvcEvent $e) {
		// Get object translator'a
		$translator = $e->getApplication()->getServiceManager()->get('translator');

		// Session container
		$session = new Container('locale');

		// test if session language exists
		if ($session->offsetExists('locale') == false) {
			// if not use the browser locale
			if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
				$session->offsetSet('locale', \Locale::acceptFromHttp($_SERVER['HTTP_ACCEPT_LANGUAGE']));
			} else {
				$session->offsetSet('locale', 'en_US');
			}
		}

		$translator->setLocale($session->language)
			->setFallbackLocale('en_US');
	}

	public function getConfig() {
		return include __DIR__ . '/config/module.config.php';
	}

	public function getAutoloaderConfig() {
		return array(
			'Zend\Loader\StandardAutoloader' => array(
				'namespaces' => array(
					__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
				),
			),
		);
	}
}
