<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use \Zend\Session\Container;

class IndexController extends BaseController
{
	public function indexAction() {
		return new ViewModel();
	}

	/**
	 * @return ViewModel
	 */
	public function changelocaleAction() {

		// disable layout
		$result = new ViewModel();
		$result->setTerminal(true);

		// variables
		$event = $this->getEvent();
		$matches = $event->getRouteMatch();
		//$language = $request->getPost()->language;
		$myLocale = $matches->getParam('locale');
		$redirect = $matches->getParam('redirecturl', '');

		// translate
		$session = new Container('locale');
		$config = $this->serviceLocator->get('config');

		if (isset($config['locale']['available'][ $myLocale ]) == false) {
			$myLocale = 'en_US';
		}

		$session->language = $myLocale;
		//$this->serviceLocator->get('translator')->setLocale($session->language);

		// redirect
		switch ($redirect) {
			case '':
				$this->redirect()->toRoute('home');
				break;
			default :
				$this->redirect()->toUrl(urldecode($redirect));
		}

		return $result;
	}
}
