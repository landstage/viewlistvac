<?php

namespace Work;

return array(

	'router'          => array(
		'routes' => array(
			'work' => array(
				'type'    => 'Segment',
				'options' => array(
					'route'       => '/work[/:action][/:id][/lang/:lang][/department/:depart][/page/:page]',
					'constraints' => array(
						'action' => '(?!\bpage\b)(?!\blang\b)(?!\bdepart\b)[a-zA-Z][a-zA-Z0-9_-]*',
						'id'     => '[0-9]+',
						'page'   => '[0-9]+',
						'lang'   => '[a-z][a-z]_[A-Z][A-Z]',
						'depart' => '[0-9]+',
					),
					'defaults'    => array(
						'__NAMESPACE__' => 'Work\Controller',
						'controller'    => 'Work',
						'action'        => 'index',
					),
				),
			),
		),
	),

	'controllers'     => array(
		'factories' => array(
			'Work\Controller\Work' => 'Work\Factory\Controller\WorkControllerFactory',
		),
	),

	'view_manager'    => array(
		'template_path_stack' => array(
			__DIR__ . '/../view',
		),
		'template_map'        => array(
			'work/partial/index' => __DIR__ . '/../view/work/work/partial/indextable.phtml',
		),
		'strategies'          => array(
			'ViewJsonStrategy',
		),
	),

	'doctrine'        => array(
		'driver' => array(
			__NAMESPACE__ . '_driver' => array(
				'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
				'cache' => 'array',
				'paths' => array(__DIR__ . '/../src/' . __NAMESPACE__ . '/Entity')
			),
			'orm_default'             => array(
				'drivers' => array(
					__NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
				)
			),
		),
	),

	'data-fixture'    => array(
		'location' => __DIR__ . '/../src/' . __NAMESPACE__ . '/Fixture',
	),

	'service_manager' => array(
		'factories' => array(
			'Work\Service\WorkService' => 'Work\Factory\Service\WorkServiceFactory',
		),
	),

	'form_elements'   => array(
		'factories' => array(
			'Work\Form\WorkForm' => 'Work\Factory\Form\WorkFormFactory',
		),
	),
);