<?php
namespace Work\Controller;

use Application\Controller\BaseController;
use Zend\Form\FormInterface;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Doctrine\ORM;
use Work\Entity;
use Work\Form;

class WorkController extends BaseController
{
	const WORKVACANCY_ENTITY = 'Work\Entity\WorkVacancy';

	/**
	 * Set form search
	 * @var $workForm \Zend\Form\Form | \Work\Form\WorkForm
	 */
	protected $workForm;

	/**
	 * @param FormInterface $workForm
	 */
	public function __construct(FormInterface $workForm) {
		$this->workForm = $workForm;
	}

	public function indexAction() {

		$this->workForm->setName('worksearch');

		/* @var $request \Zend\Http\Request */
		$request = $this->getRequest();

		$routeParams = array();
		if ($request->isPost()) {

			$routeParams = $request->getPost()->toArray();
			$page = $routeParams['page'];
			unset($routeParams['page']);

		} else {

			$routeParams = array(
				'lang'       => $this->params()->fromRoute('lang'),
				'department' => $this->params()->fromRoute('department')
			);

			// Get pagination
			$page = (int)$this->params()->fromRoute('page', 1);
			$this->workForm->setData($routeParams);
		}

		$pagedVacancy = $this->getServiceLocator()->get('Work\Service\WorkService')->getPagedVacancy($routeParams, $page);

		if ($request->isXmlHttpRequest() == true && $request->isPost() == true) {

			// Render for json html
			$htmlViewPart = new ViewModel();
			$htmlViewPart->setTerminal(true)
				->setTemplate('work/partial/index')
				->setVariables(array(
					'pagedVacancy' => $pagedVacancy,
					'page'         => $page,
					'routeParams'  => $routeParams,
				));

			$htmlOutput = $this->getServiceLocator()
				->get('viewrenderer')
				->render($htmlViewPart);

			return new JsonModel(array(
				'success' => true,
				'html'    => $htmlOutput,
			));

		} else {
			return new ViewModel (array(
				'pagedVacancy' => $pagedVacancy,
				'page'         => $page,
				'routeParams'  => $routeParams,
				'form'         => $this->workForm,
			));
		}
	}

	/**
	 * @return array|\Zend\Http\Response
	 * @throws ORM\ORMException
	 * @throws ORM\OptimisticLockException
	 * @throws ORM\TransactionRequiredException
	 */
	public function addAction() {

		$requestId = $this->params()->fromRoute('id');
		$id = (int)$this->params()->fromRoute('id', 0);

		$mode = "Edit";

		// Get the Work with the specified id.
		$work = $this->getEntityManager()->find(self::WORKVACANCY_ENTITY, $id);
		if (!$work instanceof Entity\WorkVacancy) {
			$work = new Entity\WorkVacancy();
			$mode = "Add";
		}

		// Set form search
		$this->workForm->enableFilter();
		$this->workForm->bind($work);
		if ($mode == "Add") {
			$this->workForm->get('submit')->setValue('Add');
		} else {
			$this->workForm->get('submit')->setValue('Save');
		}

		/* @var $request \Zend\Http\Request */
		$request = $this->getRequest();
		if ($request->isPost() == true) {

			$this->workForm->setData($request->getPost());

			if ($this->workForm->isValid() == true) {

				$work->exchangeArray($this->workForm->getData());
				$work->setDepartment($this->getEntityManager()->find('Work\Entity\WorkDepartment', $work->getDepartment()));

				$this->getEntityManager()->persist($work);

				if ($mode == "Add") {
					$msg = 'Vacancy successfully added';
				} else {
					$msg = 'Vacancy edit';
				}

				$this->getEntityManager()->flush();

				$this->FlashMessenger()->setNamespace(\Zend\Mvc\Controller\Plugin\FlashMessenger::NAMESPACE_INFO)
					->addMessage($msg);

				// Redirect to list of work
				return $this->redirect()->toRoute('work');
			} else {
				$message = 'Error while saving WorkVacancy';
				$this->flashMessenger()->addErrorMessage($message);
			}
		} else {

			if (empty($requestId) == false && $requestId != $id) {
				$this->flashMessenger()->addMessage(sprintf('Vacancy with id %d not existing', $id));
			}
		}

		return array(
			'id'   => $id,
			'form' => $this->workForm,
			'mode' => $mode);
	}

	public function deleteAction() {

		$success = '';
		$msg = '';
		$url = '';
		$id = 0;

		/* @var $request \Zend\Http\Request */
		$request = $this->getRequest();

		if ($request->isXmlHttpRequest() == true) {

			//$id = (int)$request->getPost('id');
			$data = $this->HRequestParams()->fromJson();

			if (isset($data['id']) == false) {
				$msg = "Vacancy doesn\'t exist";
				$success = false;
			} else {
				$id = (int)$data['id'];
			}

			try {
				$workVacancy = $this->getEntityManager()->find(self::WORKVACANCY_ENTITY, $id);
				$this->getEntityManager()->remove($workVacancy);
				$this->getEntityManager()->flush();

				$msg = "Vacancy {$id} was succesfully deleted";
				$url = $this->url()->fromRoute('work', array('action' => 'index'));
				$success = true;

			} catch (\Exception $ex) {
				$msg = 'Error deleting data';
				$success = false;
			}
		}

		return new JsonModel(array(
			'success' => $success,
			'msg'     => $msg,
			'url'     => $url,
		));
	}
}