<?php

namespace Work\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * WorkVacancyRepository
 */
class WorkVacancyRepository extends EntityRepository
{
	/**
	 * @param array $searchParams
	 * @param int $offset
	 * @param int $limit
	 * @return Paginator
	 */
	public function getPagedVacancy($searchParams = array(), $offset = 0, $limit = 10) {

		$qb = $this->_em->createQueryBuilder();

		$qb->select('u')
			->from('\Work\Entity\WorkVacancy', 'u');

		$cntIn = 0;
		foreach ($searchParams as $key => $value) {
			if (empty($value) == false) {
				if ($cntIn == 0) {
					$qb->Where("u.$key = :$key");
				} else {
					$qb->andWhere("u.$key = :$key");
				}
				$qb->setParameter(':' . $key, $value);
				$cntIn++;
			}
		}

		$qb->orderBy('u.name')
			->setMaxResults($limit)
			->setFirstResult($offset);

		$query = $qb->getQuery();

		//echo $query->getSql(); die;
		return new Paginator($query);
	}
}