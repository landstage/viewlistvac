<?php

namespace Work\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * WorkVacancy
 *
 *
 * @ORM\Table(name="work_vacancy", indexes={@ORM\Index(name="ix_work_vacancy_department_id", columns={"department_id"})})
 * @ORM\Entity(repositoryClass="Work\Entity\Repository\WorkVacancyRepository")
 */
class WorkVacancy
{

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="lang", type="string", length=10, nullable=true)
	 */
	private $lang;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="name", type="string", length=128, nullable=false)
	 */
	private $name;

	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=256, nullable=false)
	 */
	private $description;

	/**
	 * @var \Work\Entity\WorkDepartment
	 *
	 * @ORM\ManyToOne(targetEntity="Work\Entity\WorkDepartment")
	 * @ORM\JoinColumns({
	 *   @ORM\JoinColumn(name="department_id", referencedColumnName="id", onDelete="CASCADE")
	 * })
	 */
	private $department;


	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Set lang
	 *
	 * @param string $lang
	 * @return WorkVacancy
	 */
	public function setLang($lang) {
		$this->lang = $lang;

		return $this;
	}

	/**
	 * Get lang
	 *
	 * @return string
	 */
	public function getLang() {
		return $this->lang;
	}

	/**
	 * Set name
	 *
	 * @param string $name
	 * @return WorkVacancy
	 */
	public function setName($name) {
		$this->name = $name;

		return $this;
	}

	/**
	 * Get name
	 *
	 * @return string
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Set description
	 *
	 * @param string $description
	 * @return WorkVacancy
	 */
	public function setDescription($description) {
		$this->description = $description;

		return $this;
	}

	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription() {
		return $this->description;
	}

	/**
	 * Set department
	 *
	 * @param \Work\Entity\WorkDepartment $department
	 * @return WorkVacancy
	 */
	public function setDepartment(\Work\Entity\WorkDepartment $department = null) {
		$this->department = $department;

		return $this;
	}

	/**
	 * Get department
	 *
	 * @return \Work\Entity\WorkDepartment
	 */
	public function getDepartment() {
		return $this->department;
	}

	/**
	 * Populate from an array.
	 *
	 * @param array $data
	 */
	public function exchangeArray($data) {
		foreach ($data as $key => $val) {
			if (property_exists($this, $key) == true) {
				$this->$key = ($val !== null) ? $val : null;
			}
		}
	}

	/**
	 * Convert the object to an array.
	 *
	 * @return array
	 */
	public function getArrayCopy() {
		return get_object_vars($this);
	}
}
