<?php

namespace Work\Factory\Controller;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Work\Controller\WorkController;

class WorkControllerFactory implements FactoryInterface
{
	/**
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return WorkController
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {

		/**
		 * @var $serviceLocator \Zend\ServiceManager\AbstractPluginManager
		 */
		$services = $serviceLocator->getServiceLocator();
		$workForm = $services->get('FormElementManager')->get('Work\Form\WorkForm');

		return new WorkController($workForm);
	}
}