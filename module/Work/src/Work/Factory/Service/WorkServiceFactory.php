<?php

namespace Work\Factory\Service;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Work\Service\WorkService;

class WorkServiceFactory implements FactoryInterface
{
	/**
	 * @param ServiceLocatorInterface $serviceLocator
	 * @return WorkService
	 */
	public function createService(ServiceLocatorInterface $serviceLocator) {

		$entityManager = $serviceLocator->get('Doctrine\ORM\EntityManager');
		return new WorkService($serviceLocator, $entityManager);
	}
}