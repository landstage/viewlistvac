<?php

namespace Work\Fixture;

class ListTestData
{
	/**
	 * @return array
	 */
	public static function getDepartmentsName() {
		return array(
			'des'      => 'Дизайн',
			'dev'      => 'Разработка',
			'test'     => 'Тестирование',
			'adm'      => 'Администрирование',
			'sel'      => 'Продажа',
			'research' => 'Исследование'
		);
	}

	/**
	 * @return array
	 */
	public static function getListVacancy() {

		//$departmentName = self::getDepartmentsName(); array_rand($departmentName, 1);

		return array(
			array(
				'lang'          => 'ru',
				'name'          => 'Разработчик',
				'description'   => 'Требуется высоко квалифицированный сотрудник умееющий читать мысли на расстоянии, перемещать предметы, силой мысли.',
				'department_id' => 'dev'
			),
			array(
				'lang'          => 'ru',
				'name'          => 'Менеджер отдела продаж',
				'description'   => 'Профессиональные навыки:
				* ПК-уверенный пользователь
				* Наличие знаний и навыков работы с Excel, Word, Power Point
				',
				'department_id' => 'sel'
			),
			array(
				'lang'          => 'ru',
				'name'          => 'Руководитель отдела',
				'description'   => '',
				'department_id' => 'research'
			),
		);
	}

}