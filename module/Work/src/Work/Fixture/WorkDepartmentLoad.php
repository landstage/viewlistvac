<?php

namespace Work\Fixture;

use Doctrine\Common\DataFixtures\Doctrine;
use Work\Entity\WorkDepartment;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

class WorkDepartmentLoad extends AbstractFixture
{
	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager) {

		foreach (ListTestData::getDepartmentsName() as $refkey => $departmentName) {

			$$refkey = new WorkDepartment();
			$$refkey->setName($departmentName);
			$manager->persist($$refkey);
			$manager->flush();

			// Store reference to departments for Vacancy
			$this->addReference($refkey, $$refkey);
		}

		// Use new insert data, method setReference, getReference
	}
}