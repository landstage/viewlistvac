<?php

namespace Work\Fixture;

use Doctrine\Common\DataFixtures\Doctrine;
use Work\Entity\WorkVacancy;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class WorkVacancyLoad extends AbstractFixture implements DependentFixtureInterface
{

	/**
	 * Load data fixtures with the passed EntityManager
	 *
	 * @param ObjectManager $manager
	 */
	public function load(ObjectManager $manager) {

		foreach (ListTestData::getListVacancy() as $vacancy) {

			$workVacancy = new WorkVacancy();

			foreach ($vacancy as $column => $value) {

				// Make the right decision through magical methods _set but then lose in readability
				switch ($column) {
					case 'lang':
						$workVacancy->setLang($value);
						break;
					case 'name':
						$workVacancy->setName($value);
						break;
					case 'description':
						$workVacancy->setDescription($value);
						break;
					case 'department_id':
						$workVacancy->setDepartment($this->getReference($value));
						break;
				}
			}

			$manager->persist($workVacancy);
			$manager->flush();
		}
	}


	public function getDependencies() {
		return array('Work\Fixture\WorkDepartmentLoad');
	}
}