<?php
namespace Work\Form;

use Zend\Form\Form;
use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;

class WorkForm extends Form
{
	/**
	 * @var ServiceLocatorInterface $serviceLocator
	 */
	protected $serviceLocator;

	/**
	 * @var \Doctrine\ORM\EntityManager $entityManager
	 */
	protected $entityManager;

	/**
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param EntityManager $entityManager
	 */
	public function __construct(ServiceLocatorInterface $serviceLocator, EntityManager $entityManager) {
		parent::__construct();
		$this->serviceLocator = $serviceLocator;
		$this->entityManager = $entityManager;
	}

	public function init() {

		$this->add(array(
			'name' => 'id',
			'type' => 'Hidden',
		));

		$this->add(array(
			'name'       => 'name',
			'type'       => 'Text',
			'options'    => array(
				'min'   => 4,
				'max'   => 32,
				'label' => 'Name',
			),
			'attributes' => array(
				'class' => 'form-control',
			)
		));

		$this->add(array(
			'name'       => 'description',
			'type'       => 'Textarea',
			'options'    => array(
				'label' => 'Description',
			),
			'attributes' => array(
				'class' => 'form-control',
			),
		));

		$this->add(array(
			'type'       => 'Zend\Form\Element\Select',
			'name'       => 'lang',
			'attributes' => array(
				'id'      => 'langs',
				'class'   => 'form-control',
				'options' => $this->serviceLocator->get('config')['locale']['available'],
			),
			'options'    => array(
				'label'        => 'Lang',
				'empty_option' => '--- please choose ---',
			),
		));


		$this->add(array(
			'type'       => 'DoctrineModule\Form\Element\ObjectSelect',
			'name'       => 'department',
			'options'    => array(
				'label'          => 'Department',
				'object_manager' => $this->entityManager,
				'target_class'   => 'Work\Entity\WorkDepartment',
				'property'       => 'name',
				'empty_option'   => '--- please choose ---',
				'is_method'      => true,
				'find_method'    => array(
					'name' => 'getAllDepartments',
				),
			),
			'attributes' => array(
				'class' => 'form-control',
			),
		));


		$this->add(array(
			'name'       => 'submit',
			'type'       => 'Submit',
			'attributes' => array(
				'value' => 'Save',
				'id'    => 'submitbutton',
				'class' => 'btn btn-primary btn-success'
			),
		));
	}

	public function enableFilter() {
		$this->setInputFilter(new WorkInputFilter($this->serviceLocator, $this->entityManager));
	}
}