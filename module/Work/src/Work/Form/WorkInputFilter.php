<?php
namespace Work\Form;


use Zend\InputFilter\InputFilter;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;

class WorkInputFilter extends InputFilter implements ServiceLocatorAwareInterface, ObjectManagerAwareInterface
{
	/**
	 * @var ServiceLocatorInterface $serviceLocator
	 */
	protected $serviceLocator;

	/**
	 *
	 * @var \Doctrine\ORM\EntityManager $entityManager
	 */

	protected $entityManager;


	public function __construct(ServiceLocatorInterface $serviceLocator, ObjectManager $objectManager) {

		$this->setServiceLocator($serviceLocator);
		$this->setObjectManager($objectManager);

		$this->setValidators();
	}

	public function setValidators() {

		$this->add(array(
			'name'       => 'name',
			'required'   => true,
			'validators' => array(
				array(
					'name'    => 'StringLength',
					'options' => array(
						'min' => 4,
						'max' => 32,
					),
				),
			),
			'filters'    => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
		));
		$this->add(array(
			'name'       => 'description',
			'validators' => array(
				array(
					'name'    => 'StringLength',
					'options' => array(
						'encoding' => 'UTF-8',
						'max'      => 250,
					),
				),
			),
			'filters'    => array(
				array('name' => 'StripTags'),
				array('name' => 'StringTrim'),
			),
		));

		$this->add(array(
			'name'       => 'lang',
			'required'   => true,
			'validators' => array(
				array(
					'name'    => 'InArray',
					'options' => array(
						'haystack' => array_keys($this->getServiceLocator()->get('config')['locale']['available']),
						'messages' => array(
							'notInArray' => 'Please select language!'
						),
					),
				),
			),
		));

		$this->add(array(
			'name'       => 'department',
			'required'   => false,
			'validators' => array(
				array(
					'name'    => 'InArray',
					'options' => array(
						'haystack' => array_keys($this->getObjectManager()->getRepository('Work\Entity\WorkDepartment')->getAllDepartments(true)),
						'messages' => array(
							'notInArray' => 'Please select language!'
						),
					),
				),
			),
		));
	}

	/**
	 * Set service locator
	 *
	 * @param ServiceLocatorInterface $serviceLocator
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->serviceLocator = $serviceLocator;
	}

	/**
	 * Get service locator
	 *
	 * @return ServiceLocatorInterface
	 */
	public function getServiceLocator() {
		return $this->serviceLocator;
	}

	/**
	 * Set the object manager
	 *
	 * @param ObjectManager $objectManager
	 */
	public function setObjectManager(ObjectManager $objectManager) {
		$this->entityManager = $objectManager;
	}

	/**
	 * Get the object manager
	 *
	 * @return ObjectManager
	 */
	public function getObjectManager() {
		return $this->entityManager;
	}
}