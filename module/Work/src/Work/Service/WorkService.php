<?php

namespace Work\Service;

use Doctrine\ORM\EntityManager;
use Zend\ServiceManager\ServiceLocatorInterface;

class WorkService
{
	/**
	 * @var ServiceLocatorInterface $serviceLocator
	 */
	protected $serviceLocator;

	/**
	 * @var \Doctrine\ORM\EntityManager $entityManager
	 */
	protected $entityManager;

	/**
	 * @param ServiceLocatorInterface $serviceLocator
	 * @param EntityManager $entityManager
	 */
	public function __construct(ServiceLocatorInterface $serviceLocator, EntityManager $entityManager) {
		$this->serviceLocator = $serviceLocator;
		$this->entityManager = $entityManager;
	}

	public function getPagedVacancy($routeParams, $page) {

		$limit = 10;
		$offset = ($page == 0) ? 0 : ($page - 1) * $limit;

		return $this->entityManager->getRepository('Work\Entity\WorkVacancy')->getPagedVacancy($routeParams, $offset, $limit);
	}
}