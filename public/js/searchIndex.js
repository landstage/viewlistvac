$(document).ready(function () {

    var formName = 'form[name="worksearch"]';
    var formRefresh = '#refresh-work';
    var actionForm = $('form[name="worksearch"]').attr('action');

    $(document).on("change", formName + ' select', function (e) {

        //The post using ajax
        $.ajax({
            type: "POST",
            url: actionForm,
            data: $(formName).serialize(),
            dataType: "json",
            success: function (data) {

                var response = JSON.parse(JSON.stringify(data));
                if (response.success == true) {
                    $(formRefresh).fadeOut('slow', function () {
                        $(this).html(response.html).fadeIn('slow');
                    });
                }
            }
        });

    });
});