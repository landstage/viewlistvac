<?php

use Zend\ServiceManager\ServiceManager,
	Zend\Mvc\Service\ServiceManagerConfig;

class Bootstrap
{
	static public $config;
	static public $sm;
	static public $em;

	static public function go() {
		chdir(dirname(__DIR__));

		// Загружаем работу приложения
		include_once __DIR__ . '/../init_autoloader.php';
		self::$config = include 'config/application.config.php';
		putenv('ZF2_PATH=' . __DIR__ . '/../vendor/ZF2/library');

		\Zend\Mvc\Application::init(self::$config);
		self::$sm = self::getServiceManager(self::$config);
		self::$em = self::getEntityManager(self::$sm);

		$loader = new \Mockery\Loader;
		$loader->register();
	}

	static public function getServiceManager($config) {
		$serviceManager = new ServiceManager(new ServiceManagerConfig);
		$serviceManager->setService('ApplicationConfig', $config);
		$serviceManager->get('ModuleManager')->loadModules();

		return $serviceManager;
	}

	static public function getEntityManager($serviceManager) {
		//'Doctrine\ORM\EntityManager'
		return $serviceManager->get('doctrine.entitymanager.orm_default');
	}
}

Bootstrap::go();